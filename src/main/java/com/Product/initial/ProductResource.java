package com.Product.initial;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductResource {
	@Autowired
	ProductRepository productRepo;
	
	List<UpdatedProduct> updatedProduct=new ArrayList<UpdatedProduct>();
	
	@PutMapping("/changeproductvalues")
	public List<Product> updateProducts(@RequestBody List<Product> p){
		updatedProduct.clear();
		p.stream().forEach(prod->{
			UpdatedProduct updproduct=new UpdatedProduct();
			Optional<Product> oldProd=productRepo.findById(prod.getId());
			int oldValue=oldProd.get().getProductPrice();
			productRepo.save(prod);
			int newValue=prod.getProductPrice();	
			updproduct.setId(prod.getId());
			updproduct.setName(prod.getProductName());
			updproduct.setAmount(newValue-oldValue);
			updatedProduct.add(updproduct);
		});
		return productRepo.findAll();
	}
	@GetMapping("/getdeviation")
	public List<UpdatedProduct> retrieveDeviatedProduct(){
		return updatedProduct;
	}
	@GetMapping("/getinitialproduct")
	public List<Product> retrieveInitialProduct(){
		return productRepo.findAll();
	}
}
